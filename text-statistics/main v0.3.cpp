// Dear ImGui: standalone example application for GLFW + OpenGL 3, using programmable pipeline
// (GLFW is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan/Metal graphics context creation, etc.)
// If you are new to Dear ImGui, read documentation from the docs/ folder + read the top of imgui.cpp.
// Read online: https://github.com/ocornut/imgui/tree/master/docs
// loop problem

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdio.h>

#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
#include <GLFW/glfw3.h> // Will drag system OpenGL headers

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif
//
ImVec2 size(1280, 720);
ImVec2 pos(0, 0);
//页面定义
static bool tip = false;
static bool show_demo_window = true;
static bool main_menu = true;
static bool fun_window = false;
static bool error_window = false;
//标签，按钮
static wchar_t* lab[6] = { (wchar_t*)u8"单词数",(wchar_t*)u8"字符数",(wchar_t*)u8"句子数",(wchar_t*)u8"代码行数",(wchar_t*)u8"空行",(wchar_t*)u8"注释行数"};
static std::string* lab1[6] = {(std::string*)"word",(std::string*)"character",(std::string*)"sentence",(std::string*)"lines of code",(std::string*)"empty line",(std::string*)"comment line"};
static bool word = true;
static bool character = true;
static bool sentence = false;
static bool LOC = false;
static bool boundless = false;
static bool fraction = false;
static bool Emptyline = false;
static bool Commentline = false;
static bool unduplicated = false;
//
static int max = 0;
static int mode1 = 0;
static int right = 0;
static int t = 0;
static bool flag = 0;
static int error_id;
static int tip_id;
static bool C[6];
static int coml = 0;
static int loc = 0;
static int empl = 0;
static int re = 0;

static char buf[1024];
static int p_co = 2;
void home_page_ch();
void home_page_en();
void fun_page_en();
void fun_page_ch();
void error_windows(int id);
//判断字符
int judge(char c) {
    if ((c >= 48 && c <= 57) || (c >= 65 && c <= 90) || (c >= 97 && c <= 122)) {
        return 1;
    }
    else if (c>=32&&c != 95 && c != 45) {
        return 0;
    }
    else {
        return 2;
    }
}
class stata{
private:
    int *count = new int[6];
    char* R =new char[200];
    bool* mode = new bool[6];
public:
    stata(bool *mode):mode(mode){}
    ~stata();
    void statistics();
    void getroad(char* path);
    int getcount(int id);
};

stata::~stata() {  
    delete[] count;
}
void stata::getroad(char* path) {
    R = path;
}
void stata::statistics() {
    std::ifstream inFile(R);
    if (!inFile.is_open())
    {
        error_id = 2;
        error_window = true;
        main_menu = true;
        fun_window = false;
        flag = 0;
        return;
    }
    int t = 0;
    static char end = '\0';
    for (int i = 0; i <= 5; i++) {
        *(count + i) = 0;
    }
    bool flag2 = false;        
    while (inFile.getline(buf, sizeof(buf))) {
           
            if (*(mode + 3) || *(mode + 4) || *(mode + 5)) {
                bool flag1 = false;
                int i = 0;
                while (buf[i + 1] != '\0') {
                    if (!flag2) {
                        if (buf[i] == 47 && buf[i + 1] == 42) {
                            flag2 = true;
                        }
                        else if (buf[i] == 47 && buf[i + 1] == 47) {
                            count[5]++;
                            if (flag1) {
                                re++;
                            }
                            break;
                        }
                        else if (judge(buf[i]) != 2) {
                            if (!flag1) {
                                count[3]++;
                            }
                            flag1 = true;
                        }

                    }
                    else {
                        if (i == 0) {
                            count[5]++;
                        }
                        if (buf[i] == 42 && buf[i + 1] == 47) {
                            flag2 = false;
                            break;
                        }

                    }
                    i++;
                }
                
            }
            if (*mode) {
                if (judge(end) != 1 && judge(buf[0]) == 1) {
                    count[0]++;
                }
                if (judge(buf[0]) == 0 && judge(buf[1]) == 1) {
                    count[0]++;
                }
                int i = 1;
                while (buf[i + 1] != '\0') {
                    if (!judge(buf[i]) && judge(buf[i + 1])) {
                        count[0]++;
                    }
                    i++;
                }
                end = buf[i];

            } 
            if (*(mode + 1)) {
                
                int i = 0;
                while (buf[i] != '\0') {
                    count[1]++;
                    if (buf[i] == 32) {
                        count[1]--;
                    }
                    i++;
                }
            }
            if (*(mode + 2)) {
                int i = 0;
                while (buf[i] != '\0') {
                    if (buf[i] == 46||buf[i]==33||buf[i]==63||buf[i]==59) {
                        count[2]++;
                    }
                    i++;
                }
            }
            t++;
            memset(buf, '\0', sizeof(buf));
            count[4] = t - count[3] - count[5] + re;
        }
    re = 0;
    inFile.close();   
       
}
int stata::getcount(int id){
    return *(count+id);
}
//





static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}
std::string str;
static char road[200];

int main(int, char**)
{
     setlocale(LC_ALL, "chs");
     

    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    // Decide GL+GLSL versions
#if defined(IMGUI_IMPL_OPENGL_ES2)
    // GL ES 2.0 + GLSL 100
    const char* glsl_version = "#version 100";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
#elif defined(__APPLE__)
    // GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1280, 720, "text-statistics", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\msyh.ttc", 36.0f, NULL, io.Fonts->GetGlyphRangesChineseFull());
    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
    
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        if (mode1 == 1) {
            if (main_menu) {
            home_page_ch();//主页面

            }
            if (fun_window) {
            fun_page_ch();

            }
            if (error_window) {
            error_windows(error_id);//出错提示

            }  
        }
        else {
            if (main_menu) {
                home_page_en();//主页面
            }
            if (fun_window) {
                fun_page_en();
            }
            if (error_window) {
                error_windows(error_id);//出错提示
            }
        }
     

        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
void home_page_ch() {
    ImGui::SetNextWindowPos(pos);
    ImGui::SetNextWindowSize(size);
    ImGui::Begin(u8"文本统计", &main_menu);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
    if (ImGui::BeginMenu(u8"语言")) {
        if(ImGui::MenuItem(u8"中文")){
            mode1 = 1;
        }
        if(ImGui::MenuItem("English")){
            mode1 = 2;
        }
        ImGui::EndMenu();
    }
    ImGui::InputText(u8"文件路径", road, IM_ARRAYSIZE(road), 0);
    //ImGui::InputInt(u8"数值范围", &max);
    //ImGui::InputInt(u8" ",&p_co);
    ImGui::Text(u8"支持类型：");
    if (ImGui::BeginTable("split", 3)) {
        ImGui::TableNextColumn(); ImGui::Checkbox(u8"统计单词", &word);
        ImGui::TableNextColumn(); ImGui::Checkbox(u8"统计字符(不含空格)", &character);
        ImGui::TableNextColumn(); ImGui::Checkbox(u8"统计句子", &sentence);
        ImGui::TableNextColumn(); ImGui::Checkbox(u8"统计代码行", &LOC);
        ImGui::TableNextColumn(); ImGui::Checkbox(u8"统计空行", &Emptyline);
        ImGui::TableNextColumn(); ImGui::Checkbox(u8"统计注释行", &Commentline);
        ImGui::EndTable();
    }
    
    if (ImGui::Button(u8"确定")) {
         
              if (word) {
                    C[0] = 1;
              }
              else {
                    C[0] = 0;
              }
              if (character) {
                    C[1] = 1;
              }
              else {
                    C[1] = 0;
              }
              if (sentence) {
                    C[2] = 1;
              }
              else
              {
                    C[2] = 0;
              }
              if (LOC) {
                    C[3] = 1;
              }
              else
              {
                    C[3] = 0;
              }
              if (Emptyline) {
                    C[4] = 1;
              }
              else
              {
                    C[4] = 0;
              }
              if (Commentline) {
                    C[5] = 1;
              }
              else
              {
                    C[5] = 0;
              }
         
            fun_window = true;
            main_menu = false;
    }
        
    if (ImGui::Button(u8"退出")) {
        main_menu = false;
    }
   
    ImGui::End();
}
void home_page_en() {
    ImGui::SetNextWindowPos(pos);
    ImGui::SetNextWindowSize(size);
    ImGui::Begin("text", &main_menu);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
    if (ImGui::BeginMenu("language")) {
        if (ImGui::MenuItem("Chinese")) {
            mode1 = 1;
        }
        if (ImGui::MenuItem("English")) {
            mode1 = 2;
        }
        ImGui::EndMenu();
    }
    ImGui::InputText("path", road, IM_ARRAYSIZE(road), 0);
    //ImGui::InputInt(u8"数值范围", &max);
    //ImGui::InputInt(u8" ",&p_co);
    ImGui::Text("Support types:");
    if (ImGui::BeginTable("split", 3)) {
        ImGui::TableNextColumn(); ImGui::Checkbox("Count the words", &word);
        ImGui::TableNextColumn(); ImGui::Checkbox("Count character", &character);
        ImGui::TableNextColumn(); ImGui::Checkbox("Count sentence", &sentence);
        ImGui::TableNextColumn(); ImGui::Checkbox("Count lines of code ", &LOC);
        ImGui::TableNextColumn(); ImGui::Checkbox("Count Empty line", &Emptyline);
        ImGui::TableNextColumn(); ImGui::Checkbox("Count Comment line", &Commentline);
        ImGui::EndTable();
    }

    if (ImGui::Button("next")) {

        if (word) {
            C[0] = 1;
        }
        else {
            C[0] = 0;
        }
        if (character) {
            C[1] = 1;
        }
        else {
            C[1] = 0;
        }
        if (sentence) {
            C[2] = 1;
        }
        else
        {
            C[2] = 0;
        }
        if (LOC) {
            C[3] = 1;
        }
        else
        {
            C[3] = 0;
        }
        if (Emptyline) {
            C[4] = 1;
        }
        else
        {
            C[4] = 0;
        }
        if (Commentline) {
            C[5] = 1;
        }
        else
        {
            C[5] = 0;
        }

        fun_window = true;
        main_menu = false;
    }

    if (ImGui::Button(u8"back")) {
        main_menu = false;
    }

    ImGui::End();
}
void fun_page_ch(){
    
    static bool wrong = false;
    static float rate;
    ImGui::SetNextWindowPos(pos);
    ImGui::SetNextWindowSize(size);
    ImGui::Begin(u8"文本统计", &fun_window);
    


    static char buf2[10];
    static double answer;
    static char c[2];
    static int count[6];
    static stata text1(C);
    text1.getroad(road);
    if (!flag) {         
        text1.statistics(); 
        flag = true;
    }
    else {
        for (int i = 0; i < 6; i++) {
            if (C[i]) {
                ImGui::Text(u8"%s：%d ",lab[i],text1.getcount(i));
            }
        }
    }
    if (ImGui::Button(u8"返回")) {
        main_menu = true;
        fun_window = false;
        t = 0;
        flag = 0;
        memset(C, '\0', sizeof(C));
       
    }
    ImGui::End();
    
}
void fun_page_en() {
    
    static bool wrong = false;
    static float rate;
    ImGui::SetNextWindowPos(pos);
    ImGui::SetNextWindowSize(size);
    ImGui::Begin("Text statistics", &fun_window);


    static char buf2[10];
    static double answer;
    static char c[2];
    static stata text1(C);
    text1.getroad(road);

    if (!flag) {        
        text1.statistics();         
        flag = true;
    }
    else {
        for (int i = 0; i < 6; i++) {
            if (C[i]) {
                ImGui::Text("%s:%d ", lab1[i], text1.getcount(i));
            }
        }
    }
    if (ImGui::Button("back")) {
        main_menu = true;
        fun_window = false;
        t = 0;
        flag = 0;
        memset(C, '\0', sizeof(C));
        
    }
    ImGui::End();
   
}
void error_windows(int id) {
    if (id == 0) {
        ImGui::Begin("error", &error_window);
        ImGui::Text("error:num::get_num()");
        ImGui::End();
    }
    else if (id == 1) {
        ImGui::Begin("error", &error_window);
        ImGui::Text("error:num::get_char()");
        ImGui::End();
    }
    else if (id == 2) {
        ImGui::Begin("error", &error_window);
        ImGui::Text(u8"无法打开文件，请确定路径是否正确！");
        ImGui::End();
    }

}

