# text-statistics

#### 介绍
本程序基于 OpenGL3 与 imGUI ，可实现统计文本中的单词，句子，字符，代码行等功能。

[imGUI下载链接](https://github.com/ocornut/imgui)

#### 使用说明
![](https://images.gitee.com/uploads/images/2021/1010/194352_94d08226_5581165.png "2021-10-10 194335.png")
运行该文件
![](https://images.gitee.com/uploads/images/2021/1010/194947_5da40a8b_5581165.png "屏幕截图 2021-10-10 194756.png")
在path栏中输入文件路径，勾选需要统计的信息。
运行结果如下
![](https://images.gitee.com/uploads/images/2021/1010/195721_30d1b0ea_5581165.png "2021-10-10 195704.png")

#### 单元测试
![](https://images.gitee.com/uploads/images/2021/1025/170942_6af56ea8_5581165.png "2539126-20211012181054381-147196751.png")
#### 注意事项

1. 如果换行符不是'\n',本程序会出现错误。
2. 注释行统计只支持'\\\\' 和 '\\**\\'。


